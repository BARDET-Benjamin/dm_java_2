import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.isEmpty()){
            return null;
        }
        else {
          Integer min = liste.get(0);
          for (Integer elem : liste)
              if ( elem < min )
                  min = elem;
          return min;
        }
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T elem : liste){
            if ( elem.compareTo(valeur) <= 0 )
                return false;
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        int i1 = 0;
        int i2 = 0;
        ArrayList<T> liste3 = new ArrayList<>();
        while ( i1 < liste1.size() && i2 < liste2.size() ){
            if (liste1.get(i1).compareTo(liste2.get(i2)) == 0){
                if ( !liste3.contains(liste1.get(i1)) )
                  liste3.add(liste1.get(i1));
                i1++;
                i2++;
            }
            else if (liste1.get(i1).compareTo(liste2.get(i2)) < 0){
                i1 = i1 + 1;
            }
            else {
                i2 = i2 + 1;
            }
        }

        return liste3;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> mots = new ArrayList<>();
        String[] text = texte.split(" ");
        if (texte.length() == 0)
            return mots;

        for (String elem : text) {
            if (!elem.equals(""))
                mots.add(elem);
        }
        return mots;
    }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
          if (texte.length() == 0)
              return null;
          List<String> texteDecoupe = new ArrayList<>();
          texteDecoupe.addAll(decoupe(texte));
          Hashtable<String, Integer> dico = new Hashtable<>();
          String motplusPresent = "";

          for (String elem : texteDecoupe){
              if ( !dico.containsKey(elem) )
                  dico.put(elem,1);
              else{
                  dico.replace(elem, dico.get(elem)+1 );
              }
          }

          for ( String elem : dico.keySet() ){
              if ( motplusPresent.equals("") || dico.get(elem) >= dico.get(motplusPresent) ){
                  if ( dico.get(elem) == dico.get(motplusPresent) ){
                      if (elem.compareTo(motplusPresent) < 0)
                          motplusPresent = elem;
                  }
                  else{
                      motplusPresent = elem;
                  }
              }
          }

        return motplusPresent;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int nbP = 0;
        for (int i = 0; i < chaine.length(); i++){
            if (chaine.charAt(i) == '(' )
                nbP++ ;
            if (chaine.charAt(i) == ')' ){
                if (nbP == 0)
                    return false;
                nbP--;
            }
        }
        if (nbP == 0)
            return true;
        return false;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){

        int nbP = 0;
        int nbC = 0;

        for (int i = 0; i < chaine.length(); i++){

            if (chaine.charAt(i) == '(' )
                nbP++ ;

            if (chaine.charAt(i) == ')' ){
                if (nbP == 0)
                    return false;
                if (nbC > 0 && chaine.charAt(i-1) != '(')
                    return false;
                nbP--;
            }


            if (chaine.charAt(i) == '[' )
                nbC++ ;

            if (chaine.charAt(i) == ']' ){
                if (nbC == 0)
                    return false;
                if (nbP > 0 && chaine.charAt(i-1) != '[')
                    return false;
                nbC--;
            }

        }

        if (nbC == 0 && nbP == 0)
            return true;
        return false;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.size() == 0)
            return false;
        int i = liste.size() / 2;
        Integer prec = null;
        boolean ok = false;
        while (!ok){
            if (liste.get(i) == valeur)
                ok = true;
            if (liste.get(i) > valeur-3 && liste.get(i) < valeur+3)
                for (int i2 = i-2; i2<6+i; i2++){
                    if (i2 < 0 || i2 > liste.size()-1)
                        i2=i+7;
                    else if (liste.get(i2) == valeur)
                        ok = true;
                }
            else if (prec != null && liste.get(i) == prec)
                return false;


            else if (liste.get(i) < valeur)
                i = i / 2;
            else if (liste.get(i) > valeur)
                i = i + i / 2;
            prec = liste.get(i);
            }
        return ok;
        }



}
